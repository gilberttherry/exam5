package com.apiTest.division.base;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.PropertyConfigurator;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import com.apiTest.division.testCases.TC_001_Get_All_Divisions;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DivisionTestBase {
	protected static RequestSpecification httpRequest;
	protected static Response response;
	String endpoint = "/company/1/division";
	protected Logger logger;
	
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public int getLength(String list){
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpoint);
		return response.jsonPath().getList(list).size();
	}
	@BeforeClass
	public void setup() {
		logger = Logger.getLogger("DivisionRestAPI");
		PropertyConfigurator.configure("Log4j.properties");
	}
	
	public void emailValid() {
		JsonPath jsonPath = response.jsonPath();
		String email = jsonPath.get("email");
		logger.info(email);
		
		String regex = "^(.+)@(.+)$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	public void nameValid() {
		JsonPath jsonPath = response.jsonPath();
		String name = jsonPath.get("name");
		logger.info(name);
		
		String regex = "/^[A-Za-z]+/";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(name);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	public void checkEmptyBody() {
		logger.info("******** Checking is Response Body Empty *********");

		String responseBody = response.getBody().asString();
		logger.info("response Body ===> "+responseBody);
		Assert.assertTrue(responseBody != null);
	}
	
	public void checkStatusCode(String statusCodeParam) {
		logger.info("******** Checking Status Code *********");
		
		int statusCode = response.getStatusCode();
		logger.info("Status Code ===> "+ statusCode);
		Assert.assertEquals(statusCode, Integer.parseInt(statusCodeParam));
	}
	
	public void checkResponseBody(String name, org.json.simple.JSONObject divHead, org.json.simple.JSONArray employee,String email) {
		logger.info("******** Checking Response Body *********");
		
		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody.contains(name), true);
		Assert.assertEquals(responseBody.contains(employee.toString()), true);
		Assert.assertEquals(responseBody.contains(divHead.toString()),true);
		Assert.assertEquals(responseBody.contains(email), true);
	}
	
	public void checkResponseBodyEmployee(String name, org.json.simple.JSONObject head,JSONArray employee,String email) {
		logger.info("******** Checking Response Body *********");
		
		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody.contains(name), true);
		Assert.assertEquals(responseBody.contains(employee.toString()), true);
		Assert.assertEquals(responseBody.contains(head.toString()),true);
		Assert.assertEquals(responseBody.contains(email), true);
	}

	public void checkResponseBodyHead(String name, JSONObject head, org.json.simple.JSONArray employee,String email) {
		logger.info("******** Checking Response Body *********");
		
		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody.contains(name), true);
		Assert.assertEquals(responseBody.contains(employee.toString()), true);
		Assert.assertEquals(responseBody.contains(head.toString()),true);
		Assert.assertEquals(responseBody.contains(email), true);
	}
	public void checkUpdated(String name, org.json.simple.JSONObject divHead, org.json.simple.JSONArray employee, String email) {
		logger.info("******** Checking Body Updated or Not *********");

		String responseBody = response.getBody().asString();
		Assert.assertTrue(responseBody.contains(name));
		Assert.assertTrue(responseBody.contains(employee.toString()));
		Assert.assertTrue(responseBody.contains(divHead.toString()));
		Assert.assertTrue(responseBody.contains(email));
	}
	
	public void checkResponseBodyDynamicPost(String name, JSONObject divHead, JSONArray employee, String email) {
		logger.info("******** Checking Response Body Dynamic Post *********");
		
		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody.contains(name), true);
		Assert.assertEquals(responseBody.contains(employee.toString()), true);
		Assert.assertEquals(responseBody.contains(divHead.toString()),true);
		Assert.assertEquals(responseBody.contains(email), true);
	}
	public void checkResponseTime(String responseTimeParam) {
		logger.info("******** Checking Response Time *********");
		
		long responseTime = response.getTime();
		logger.info("Response time ===> "+ responseTime);
		Assert.assertTrue(responseTime < Integer.parseInt(responseTimeParam));
	}
	
	public void checkStatusLine(String statusLineParam) {
		logger.info("******** Checking Status Line *********");
		
		String statusLine = response.getStatusLine();
		logger.info("Status line ===> " + statusLine);
		Assert.assertEquals(statusLine, statusLineParam);
	}
	public void checkContentType(String contentTypeParam) {
		logger.info("******** Checking Content Type *********");
		
		String contentType = response.header("Content-Type");
		logger.info("Content-Type ===>" + contentType);
		Assert.assertEquals(contentType, contentTypeParam);
	}
	public void checkServerType(String serverTypeParam) {
		logger.info("******** Checking Server Type *********");
		
		String serverType = response.header("Server");
		logger.info("Server Type ===>" + serverType);
		Assert.assertEquals(serverType, serverTypeParam);
	}
	public void checkContentEncoding(String contentEncodingParam) {
		logger.info("******** Checking Content Encoding *********");
		
		String contentEncoding = response.header("Content-Encoding");
		logger.info("Content Encoding ===> "+ contentEncoding);
		Assert.assertEquals(contentEncoding, contentEncodingParam);
	}
	public void checkContentLength(String contentLengthParam) {
		logger.info("******** Checking Content Length *********");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content-Length ===> " + contentLength);
		if(Integer.parseInt(contentLength) < Integer.parseInt(contentLengthParam))
			logger.warning("Content Length is less than 50");
		Assert.assertTrue(Integer.parseInt(contentLength) >= Integer.parseInt(contentLengthParam));
	}
	public void checkContentLengthAll(String contentLengthParam) {
		logger.info("******** Checking Content Length *********");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content-Length ===> " + contentLength);
		if(contentLength == null)
			logger.info("Content Length is null");
		else {
			if(Integer.parseInt(contentLength) < Integer.parseInt(contentLengthParam))
				logger.warning("Content Length is less than 50");
			Assert.assertTrue(Integer.parseInt(contentLength) >= Integer.parseInt(contentLengthParam));	
		}
	}
	public void checkCookies(String cookiesParam) {
		logger.info("******** Checking Cookie *********");
		
		String cookie = response.getCookie("PHPSESSID");
		System.out.println(cookie);
		Assert.assertEquals(cookie, cookiesParam);
	}
	public void jsonValidator() {
		logger.info("******** Validating JSON *********");
		JSONObject jsonSchema = new JSONObject(
			      new JSONTokener(DivisionTestBase.class.getResourceAsStream("/division.json")));
		JSONArray jsonSubject = new JSONArray(
			      new JSONTokener(response.getBody().asString()));
			     
		Schema schema = SchemaLoader.load(jsonSchema);
		schema.validate(jsonSubject);		 
	}
	public void jsonValidatorSingleObject() {
		JSONObject jsonSchema = new JSONObject(
			      new JSONTokener(DivisionTestBase.class.getResourceAsStream("/division.json")));
		JSONObject jsonSubject = new JSONObject(
			      new JSONTokener(response.getBody().asString()));
		JSONArray jsonArray = new JSONArray();
		jsonSubject.put("id",jsonArray);
		 
		Schema schema = SchemaLoader.load(jsonSchema);
		schema.validate(jsonArray);	
	}
}
