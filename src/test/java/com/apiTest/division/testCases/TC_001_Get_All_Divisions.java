package com.apiTest.division.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_All_Divisions extends DivisionTestBase{
	
	@BeforeClass
	@Parameters("baseURI")
	private void getAllEmployees(String baseURIParam) throws InterruptedException{
		logger.info("******** Started TC_001__Get_All_Divisions *********");
		RestAssured.baseURI = baseURIParam;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, getEndpoint());
		
		Thread.sleep(3);
	}
	
	@Test
	private void checkResponseBodyGetAll() {
		checkEmptyBody();
	}
	
	@Test
	@Parameters("statusCode")
	private void checkStatusCodeGetAll(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimeGetAll(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("contentLength")
	private void checkContentLengthGetAll(String contentLengthParam) {
		checkContentLengthAll(contentLengthParam);
	}
	
	@Test
	@Parameters("statusLine")
	private void checkStatusLineGetAll(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypeGetAll(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
	
	@Test
	@Parameters("serverType")
	private void checkServerTypeGetAll(String serverTypeParam) {
		checkServerType(serverTypeParam);
	}
	
	@Test
	@Parameters("contentEncoding")
	private void checkContentEncodingGetAll(String contentEncodingParam) {
		checkContentEncoding(contentEncodingParam);
	}
	
	
	@Test
	@Parameters("cookies")
	void checkCookiesGetAll(String cookiesParam) {
		checkCookies(cookiesParam);
	}
	
	@Test
	private void jsonTestingUsingSchema() {
		jsonValidator();		 
	}
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_001__Get_All_Divisions *********");
		
	}
}
