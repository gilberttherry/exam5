package com.apiTest.division.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_002_Get_Single_Division_Record extends DivisionTestBase{
	@BeforeClass
	@Parameters("baseURI")
	private void getEmployee(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_002_Get_Single_Division_Record *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, getEndpoint()+"/"+getLength("division"));
		Thread.sleep(3);
	}
	
	@Test
	@Parameters("statusCode")
	private void checkStatusCodeGetSingle(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimeGetSingle(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}
	@Test
	private void checkEmailValid() {
		emailValid();
	}
	
	@Test
	private void checkNameValid() {
		nameValid();
	}
	@Test
	@Parameters("statusLine")
	private void checkStatusLineGetSingle(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}

	@Test
	@Parameters("serverType")
	private void checkServerTypeGetSingle(String serverTypeParam) {
		checkServerType(serverTypeParam);
	}
	
	@Test
	@Parameters("contentEncodingNull")
	private void checkContentEncodingGetSingle(String contentEncodingParam) {
		checkContentEncoding(contentEncodingParam);
	}

	
	@Test
	@Parameters("contentType")
	private void checkContentTypeGetAll(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
	
	@Test
	@Parameters("contentLength")
	private void checkContentLengthGetSingle(String contentLengthParam) {
		checkContentLength(contentLengthParam);
	}
	
	@Test
	private void jsonTestingUsingSchema() {
		jsonValidatorSingleObject();		 
	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_002_Get_Single_Division_Record *********");
		
	}
}
