package com.apiTest.division.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_005_Delete_Division_Record extends DivisionTestBase{
	
	@BeforeClass
	@Parameters("baseURI")
	private void deleteEmployeeData(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_005_Delete_Division_Record *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, getEndpoint());
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		
		String id = jsonPathEvaluator.get("[0].id");
		response = httpRequest.request(Method.DELETE, getEndpoint()+"/"+getLength("division"));
		
		Thread.sleep(3);
	}
	


	@Test
	@Parameters("statusCode")
	private void checkStatusCodeDelete(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimeDelete(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLineDelete(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypeDelete(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
	
	@Test
	@Parameters("serverType")
	private void checkServerTypeDelete(String serverTypeParam) {
		checkServerType(serverTypeParam);
	}
	
	@Test
	@Parameters("contentEncodingNull")
	private void checkContentEncodingDelete(String contentEncodingParam) {
		checkContentEncoding(contentEncodingParam);
	}
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_005_Delete_Division_Record *********");
		
	}
}
