package com.apiTest.division.testCases;

import java.io.IOException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;
import com.apiTest.division.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_006_Post_Using_Excel extends DivisionTestBase{
	private org.json.JSONObject jsonHead;	
	private JSONArray jsonEmployee;
	private String name;
	private String email;
	
	@BeforeClass
	@Parameters("baseURI")
	private void PostEmployeeData(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_006_Post_Using_Excel *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();

		Thread.sleep(3);
	}
	
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataemployes", priority = 0)
	private void dataDrivenPost(String ename, String email, String eemployee, String ehead) throws JSONException, InterruptedException {
		httpRequest = RestAssured.given();

		JSONObject RequestParams = new JSONObject();
		
		String escapedString = StringEscapeUtils.unescapeJava(eemployee);
		String escapedString2 = StringEscapeUtils.unescapeJava(ehead);
		jsonEmployee = new JSONArray(escapedString);
		jsonHead = new org.json.JSONObject(escapedString2);
		name=ename;
		this.email=email;
		RequestParams.put("name",ename);
		RequestParams.put("email", email);
		RequestParams.put("employee",jsonEmployee);
		RequestParams.put("head",jsonHead);
		
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		
		response = httpRequest.request(Method.POST, getEndpoint());
		
		Thread.sleep(5);
	}
	
	@DataProvider(name="dataemployes")
	private String[][] getDataEmpty() throws IOException {
		String path = "/Users/therryg/eclipse-workspace/Exam5/src/test/java/com/apiTest/division/testCases/Sheet1.xlsx";
		
		int rownum =Utility.getRowCount(path, "Sheet1");
		int colcount = Utility.getCellCount(path, "Sheet1",1);
		String dataEmployes [][] = new String[rownum][colcount];
		for(int i=1;i<=rownum;i++) {
			for(int j=0;j<colcount;j++) {
				dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
			}
		}
		return(dataEmployes);
	}

	@Test(priority = 1)
	private void checkResponseBodyPost() {
		checkResponseBodyDynamicPost(name, jsonHead, jsonEmployee, email);
	}
	
	
	@Test(priority = 1)
	@Parameters("postStatusCode")
	private void checkStatusCodePost(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test(priority = 1)
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test(priority = 1)
	@Parameters("postStatusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test(priority = 1)
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
	
	@Test(priority = 1)
	@Parameters("serverType")
	private void checkServerTypePost(String serverTypeParam) {
		checkServerType(serverTypeParam);
	}
	
	@Test(priority = 1)
	@Parameters("contentEncodingNull")
	private void checkContentEncodingPost(String contentEncodingParam) {
		checkContentEncoding(contentEncodingParam);
	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_006_Post_Using_Excel *********");
		
	}
}
