package com.apiTest.division.testCases;

import org.json.JSONTokener;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;
import com.apiTest.division.utilities.RestUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_011_Post_With_Wrong_Head_Data_Type extends DivisionTestBase{
	private String name = RestUtils.divName();
	private int divHead = 4;
	private JSONArray employee = RestUtils.employee();
	private String email = RestUtils.email(name);
	
	@BeforeClass
	@Parameters("baseURI")
	private void PostEmployeeData(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_011_Post_With_Wrong_Head_Data_Type *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("employee", employee);
		requestParams.put("head", divHead);
		requestParams.put("email", email);
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, getEndpoint());

		Thread.sleep(3);
	}
	
	@Test
	private void checkResponseBodyPost() {
		//this should return false, because divHead is JSONObject but this time it entered as integer
		org.json.JSONObject head = new org.json.JSONObject(
			      new JSONTokener(Integer.toString(divHead)));
		try {
			checkResponseBodyHead(name, head, employee,email);
			Assert.assertTrue(false);
		} catch (Exception e) {
			Assert.assertTrue(true);
		}
	}
	
	
	@Test
	@Parameters("forbiddenStatusCode")
	private void checkStatusCodePost(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
	}

	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
	
	@Test
	@Parameters("serverType")
	private void checkServerTypePost(String serverTypeParam) {
		checkServerType(serverTypeParam);
	}
	
	@Test
	@Parameters("contentEncodingNull")
	private void checkContentEncodingPost(String contentEncodingParam) {
		checkContentEncoding(contentEncodingParam);
	}
	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_011_Post_With_Wrong_Head_Data_Type *********");
		
	}
}
