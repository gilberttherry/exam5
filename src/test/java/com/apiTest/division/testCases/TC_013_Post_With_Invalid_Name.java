package com.apiTest.division.testCases;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;
import com.apiTest.division.utilities.RestUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_013_Post_With_Invalid_Name extends DivisionTestBase{
	private String name = "asda*&%(12,.31";
	private JSONObject divHead = RestUtils.divHead();
	private JSONArray employee = RestUtils.employee();
	private String email = RestUtils.email(name);
	
	@BeforeClass
	@Parameters("baseURI")
	private void PostEmployeeData(String baseUriPAram) throws InterruptedException{
		logger.info("******** Started TC_013_Post_With_Invalid_Name *********");
		RestAssured.baseURI = baseUriPAram;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("employee", employee);
		requestParams.put("head", divHead);
		requestParams.put("email", email);
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, getEndpoint());

		Thread.sleep(3);
	}
	
	@Test
	private void checkResponseBodyPost() {
		//function checkResponseBody should error because the inputted name format is invalid
		try {
			checkResponseBody(name, divHead, employee,email);
			Assert.assertTrue(false);
		} catch (Exception e) {
			Assert.assertTrue(true);
		}
	}
	
	
	@Test
	@Parameters("forbiddenStatusCode")
	private void checkStatusCodePost(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
	
	@Test
	@Parameters("serverType")
	private void checkServerTypePost(String serverTypeParam) {
		checkServerType(serverTypeParam);
	}
	
	@Test
	@Parameters("contentEncodingNull")
	private void checkContentEncodingPost(String contentEncodingParam) {
		checkContentEncoding(contentEncodingParam);
	}
	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_013_Post_With_Invalid_Name *********");
		
	}
}
