package com.apiTest.division.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_019_Get_With_Negative_Company_Id extends DivisionTestBase{
	@BeforeClass
	@Parameters("baseURI")
	private void getEmployee(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_019_Get_With_Negative_Company_Id *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, getEndpoint()+"/-"+(getLength("division")+1));
		Thread.sleep(3);
	}
	
	@Test
	@Parameters("404StatusCode")
	private void checkStatusCodeGetSingle(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimeGetSingle(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_019_Get_With_Negative_Company_Id *********");
		
	}
}
