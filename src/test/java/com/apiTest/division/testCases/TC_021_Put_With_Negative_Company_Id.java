package com.apiTest.division.testCases;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;
import com.apiTest.division.utilities.RestUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_021_Put_With_Negative_Company_Id extends DivisionTestBase{
	private String name = RestUtils.divName();
	private JSONObject divHead = RestUtils.divHead();
	private JSONArray employee = RestUtils.employee();
	private String email = RestUtils.email(name);
	
	@BeforeClass
	@Parameters("baseURI")
	private void PutEmployeeData(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_021_Put_With_Negative_Company_Id *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("employee", employee);
		requestParams.put("head", divHead);
		requestParams.put("email", email);
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.PUT, getEndpoint()+"/-"+(getLength("division")+1));

		Thread.sleep(3);
	}
	
	@Test
	@Parameters("404StatusCode")
	private void checkStatusCodePut(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePut(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_021_Put_With_Negative_Company_Id *********");
		
	}
}
