package com.apiTest.division.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.division.base.DivisionTestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_022_Delete_With_Not_Available_Company_Id extends DivisionTestBase{
	
	@BeforeClass
	@Parameters("baseURI")
	private void deleteEmployeeData(String baseUriParam) throws InterruptedException{
		logger.info("******** Started TC_005_Delete_Division_Record *********");
		RestAssured.baseURI = baseUriParam;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.DELETE, getEndpoint()+"/"+(getLength("division")+1));
		
		Thread.sleep(3);
	}
	


	@Test
	@Parameters("404StatusCode")
	private void checkStatusCodeDelete(String statusCodeParam) {
		checkStatusCode(statusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimeDelete(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Finished TC_005_Delete_Division_Record *********");
		
	}
}
