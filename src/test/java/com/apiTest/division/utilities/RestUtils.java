package com.apiTest.division.utilities;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class RestUtils {
	public static String divName() {
		String generateString = RandomStringUtils.randomAlphabetic(1);
		return ("John"+ generateString);
	}
	public static JSONObject divHead() {
		JSONObject jsonObject = new JSONObject();
		String generateName = RandomStringUtils.randomAlphabetic(4);
		int generateAge = Integer.parseInt(RandomStringUtils.randomNumeric(2));
		jsonObject.put("name", generateName);
		jsonObject.put("age", generateAge);
		return jsonObject;
	}

	public static JSONObject InvalidNameFormatDivHead() {
		JSONObject jsonObject = new JSONObject();
		String generateName = "kasj2232*(&!@#!@.,";
		int generateAge = Integer.parseInt(RandomStringUtils.randomNumeric(2));
		jsonObject.put("name", generateName);
		jsonObject.put("age", generateAge);
		return jsonObject;
	}

	public static JSONObject WrongNameDataTypeDivHead() {
		JSONObject jsonObject = new JSONObject();
		int generateName = Integer.parseInt(RandomStringUtils.randomNumeric(4));
		int generateAge = Integer.parseInt(RandomStringUtils.randomNumeric(2));
		jsonObject.put("name", generateName);
		jsonObject.put("age", generateAge);
		return jsonObject;
	}
	public static JSONObject WrongAgeDataTypeDivHead() {
		JSONObject jsonObject = new JSONObject();
		String generateName = RandomStringUtils.randomAlphabetic(4);
		String generateAge = RandomStringUtils.randomAlphabetic(2);
		jsonObject.put("name", generateName);
		jsonObject.put("age", generateAge);
		return jsonObject;
	}
	public static JSONObject BigNumberAtAgeDivHead() {
		JSONObject jsonObject = new JSONObject();
		String generateName = RandomStringUtils.randomAlphabetic(4);
		int generateAge = 9999999;
		jsonObject.put("name", generateName);
		jsonObject.put("age", generateAge);
		return jsonObject;
	}
	public static JSONObject NegativeAgeDivHead() {
		JSONObject jsonObject = new JSONObject();
		String generateName = RandomStringUtils.randomAlphabetic(4);
		int generateAge = Integer.parseInt(RandomStringUtils.randomNumeric(2));
		generateAge *= -1;
		jsonObject.put("name", generateName);
		jsonObject.put("age", generateAge);
		return jsonObject;
	}
	public static JSONArray employee() {
		JSONArray jsonArray = new JSONArray();
		int employee = Integer.parseInt(RandomStringUtils.randomNumeric(1));
		for(int i=0;i<employee;i++)
		{
			String generateName = RandomStringUtils.randomAlphabetic(4);
			jsonArray.add(generateName);
		}
		return jsonArray;
	}
	public static String email(String divName) {
		return (divName+"@mail.com");
	}
}
